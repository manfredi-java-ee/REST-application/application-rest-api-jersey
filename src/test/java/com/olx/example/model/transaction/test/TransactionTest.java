package com.olx.example.model.transaction.test;

import junit.framework.Assert;

import org.junit.After;
//import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.olx.example.model.transaction.TransactionInternational;
import com.olx.example.model.transaction.TransactionNational;

public class TransactionTest {
	
	
	private TransactionInternational transactionInternational = new TransactionInternational();
	private TransactionNational transactionNational = new TransactionNational();
	
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
 
	@Test
	public void testCalcularImpuestoInternational() {
		
		float montoAtransferir = 500;
		
		float resultadoEsperado = (float) (montoAtransferir * 0.05);
		
		
		float resultadoObtenido= transactionInternational.calcularImpuesto(montoAtransferir);
		
		Assert.assertEquals(resultadoObtenido,resultadoEsperado);
 
	}
	
 
	@Test
	public void testCalcularImpuestoNacional() {
		
		float montoAtransferir = 200;
		
		float resultadoEsperado = (float) (montoAtransferir * 0.01);
		
		
		float resultadoObtenido= transactionNational.calcularImpuesto(montoAtransferir);
		
		Assert.assertEquals(resultadoObtenido,resultadoEsperado);
 
	}
	
	
}
