package com.olx.example.rest;
 
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.olx.example.model.account.Account;
import com.olx.example.model.transaction.ProcessTransaction;
import com.olx.example.model.transaction.Transaction;
 
@Path("/api")
public class OlxService {
 
	 

	private static List<Account> listAccounts = new ArrayList<Account>() {

		private static final long serialVersionUID = 3762464375344384393L;
			{
		        add(new Account(1,"Frances","Argentina",1500));
		        add(new Account(2,"Frances","Brasil",2000));
		        add(new Account(3,"RIO","Argentina",3000));
		    }
		};

		
		private static List<Transaction> listTransaction = new ArrayList<Transaction>() {
			private static final long serialVersionUID = 3762464375344384393L;
	 
		};
		
	
 
	
	@GET
	@Path("/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMsg(@PathParam("param") String msg) {
 
		String output = "Jersey say : " + msg;
 
		return Response.status(200).entity(output).build();
		
	} 
	
	
	
	@GET
	@Path("/account/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Account getAccount(@PathParam("id") String id)	throws ClassNotFoundException, IOException {

		boolean encontro = false;

		Account acc = null;

		for (Account a : listAccounts) {

			if (a.getId() == Integer.parseInt(id)) {
				acc = a;
				encontro = true;
			}
		}

		if (encontro) {
			return acc;
		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

	}
	
	
	
	
 
	@POST
	@Path("/transaction")
	@Produces(MediaType.APPLICATION_JSON)
	public Response processTransaction(Transaction transaction) throws ClassNotFoundException, IOException {
		
		
		String output = "";
		
		//1  Agregar transaccion a la lista
		
		listTransaction.add(transaction);
		
		
		//2   Verifico la cantidad de transacciones a procesar encoladas.
		 
		int transacctionUnprocesed = listTransaction.size();
		
		if(transacctionUnprocesed>=1)
		{
			//proceso todas las transacciones encoladas en el archivo 
		
			List<Transaction> transactionsList = listTransaction;
			
			ExecutorService executor = Executors.newFixedThreadPool(10);
					
			for (Transaction t : transactionsList) {
			
				Runnable worker = new ProcessTransaction(t,listAccounts);
				executor.execute(worker);
			} 
			executor.shutdown();
		
			while (!executor.isTerminated())
			{
				
			}
			
			System.out.println("Finished all thread ");
			
			//borro la lista de transacciones encoladas.
			
			listTransaction.clear();
			
			output = "Se procesaron : " + transacctionUnprocesed + " transacciones.";
		}
		else
		{
			output = "Una transaccion se a encolado para ser procesada.";
		}
		
		return Response.status(200).entity(output).build();
		
	}
	
	

	
	
	
	
 
}