package com.olx.example.model.account;

import java.io.Serializable;

public class Account implements Serializable {

 
	private static final long serialVersionUID = 1L;
	 
	
	private int id ;
	private String banco;
	private String pais;
	private float saldo;
	 
	
	public Account() {
		super();
		 
	}
	public Account(int id, String banco, String pais, float saldo) {
		super();
		this.id = id;
		this.banco = banco;
		this.pais = pais;
		this.saldo = saldo;
	}

 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public float getSaldo() {
		return saldo;
	}
	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
 
	
	
	
    @Override
 		public String toString() {
		 
		    		return new StringBuffer(" ID: ")
		    		.append(this.id)
		            .append(" BANCO : ")
		            .append(this.banco)
		            .append(" PAIS : ")
		            .append(this.getPais())
		            .append(" SALDO : ")
		            .append(this.saldo).toString();
		 
		    	}

	
	
    
    
    
    
    
    
    
	
	
	
}
