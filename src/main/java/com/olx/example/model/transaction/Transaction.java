package com.olx.example.model.transaction;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import com.olx.example.model.account.Account;

 

public  abstract class  Transaction  implements Serializable  {

	
	private static final long serialVersionUID = 1L;
 
	private Account cuentaOrigen;
	private Account cuentaDestino;
	private float montoTransferido;
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private String status;
 
	
	
	public Account getCuentaOrigen() {
		return cuentaOrigen;
	}

	public void setCuentaOrigen(Account cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	public Account getCuentaDestino() {
		return cuentaDestino;
	}

	public void setCuentaDestino(Account cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}
 
	public float getMontoTransferido() {
		return montoTransferido;
	}


	public void setMontoTransferido(float montoTransferido) {
		this.montoTransferido = montoTransferido;
	}
	
    @Override
 		public String toString() {
		 
		    		return new StringBuffer("| ID CUENTA DESTINO: ")
		            .append(this.getCuentaDestino().getId())
		            .append("| ID CUENTA ORIGEN: ")
		            .append(this.getCuentaOrigen().getId())
		            .append("| MONTO TRANSFERENCIA ")
		            .append(this.montoTransferido)
		            .append("| ESTADO: ")
		            .append(this.getStatus())
		            .toString();
		 
		    	}
 
	
    public abstract float calcularImpuesto(float monto);

	 
	
}
