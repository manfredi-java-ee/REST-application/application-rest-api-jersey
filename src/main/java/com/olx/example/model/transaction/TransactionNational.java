package com.olx.example.model.transaction;

import java.io.Serializable;

public class TransactionNational extends Transaction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public float calcularImpuesto(float monto) {
	 
		return (float) (monto * 0.01);
	}

}
