package com.olx.example.model.transaction;

import java.util.List;

import org.apache.log4j.Logger;

import com.olx.example.model.account.Account;
import com.olx.example.service.transaction.TransactionService;




public class ProcessTransaction  implements Runnable  {
	
	
	private final Logger LOGGER = Logger.getLogger(ProcessTransaction.class);
	

		private TransactionService iTransactionService;
		private Transaction transaction; 
		private List<Account> accounts;
		
		
		public ProcessTransaction(Transaction itransaction,List<Account> listAccounts) {
		 		this.transaction = itransaction;
		 		this.accounts = listAccounts;

		  		
		}

		
		@Override
		public void run() {
			
		 
			
			
			
			
			iTransactionService = new TransactionService();
			
			
		 // 1 validar existencia de cuentas.
	  		
			if(!iTransactionService.validateAccountOrigen(transaction,accounts))
			{
				
				 transaction.setStatus("(NO-OK) NO SE ENCONTRO CUENTA ORIGEN");
				 LOGGER.info(transaction.toString());
	 
			}
			else
			
			if(!iTransactionService.validateAccountDestino(transaction,accounts))
			{
			// grabo la transaccion en el archivo y no la proceso 
				 transaction.setStatus("(NO-OK) NO SE ENCONTRO CUENTA DESTINO");
				 LOGGER.info(transaction.toString());
			}
			else
			{
				   // calculo impuesto
	
					float impuesto = 0;
				
					// si los bancos no son iguales se calcula el impuesto
					if (!transaction.getCuentaDestino().getBanco().equals(transaction.getCuentaOrigen().getBanco()))
				    {
						if (transaction.getCuentaDestino().getPais().equals(transaction.getCuentaOrigen().getPais())) {
							
							transaction  = new TransactionNational ();
							impuesto = transaction.calcularImpuesto(transaction.getMontoTransferido());
				
						} else {
							
							transaction = new TransactionInternational ();
							impuesto = transaction.calcularImpuesto(transaction.getMontoTransferido());
						 
						}
					}
					
					
					
					boolean saldo = true;
					
					
					saldo = iTransactionService.verificarSaldoOrigen(transaction.getCuentaOrigen(),transaction.getMontoTransferido(),impuesto,accounts);
					
					
					if(!saldo)
				  	{
					
						
						transaction.setStatus("(NO-OK) LA CUENTA ORIGEN NO TIENE SALDO SUFICIENTE PARA REALIZAR LA OPERACION");
						LOGGER.info(transaction.toString());
			
			    	} 
					else
					{
					
						// 1  calculo saldo origen 
						
						iTransactionService.calcularSaldoOrigen(transaction.getCuentaOrigen(),transaction.getMontoTransferido(),impuesto,accounts);
						
						// 2 calculo saldo destino 
						
					   	iTransactionService.calcularSaldoDestino(transaction.getCuentaDestino(),transaction.getMontoTransferido(),accounts);
						transaction.setStatus("PROCESADA-OK");
						LOGGER.info(transaction.toString());
						
					}
		
			
		 	
		}
	
		}
		/*
		
		public void calcularSaldoDestino(Account acc, float monto)
		{	
			float saldo = 0;
		
			for (Account a : accounts) {

				if (a.getId() ==  acc.getId()) {
					saldo = monto + a.getSaldo();
					a.setSaldo(saldo);
				}
			}
			
		}
		
		
	 	
		public void calcularSaldoOrigen(Account acc,float montoTransferido, float impuesto)
		{	
			float saldo = 0;

				for (Account a : accounts) {
	
					if (a.getId() ==  acc.getId()) {
						saldo = a.getSaldo() - montoTransferido -  impuesto;
						a.setSaldo(saldo);
					}
			}
		}
		
		
		
		
		public boolean validateAccountDestino(Transaction t)
		{
			
			
			
			boolean encontro  = false ;
			
			Account a = t.getCuentaDestino();
			
			for (Account cta : accounts) {
				
				if(a.getId() ==cta.getId())
				{
					t.setCuentaDestino(cta); 
					encontro = true;
				}
			}
				
			return  encontro;
					
		}
		
		
		public boolean validateAccountOrigen(Transaction t)
		{		
			
			boolean encontro  = false ;
			
			Account a = t.getCuentaOrigen();
			
			for (Account cta : accounts) {
				
				if(a.getId() ==cta.getId())
				{
					t.setCuentaOrigen(cta); 
					encontro = true;
				}
			}
				
			return  encontro;
					
		}
		
		*/
	 	
		
		
	
}
