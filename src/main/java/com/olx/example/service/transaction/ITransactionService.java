package com.olx.example.service.transaction;

import java.util.List;

import com.olx.example.model.account.Account;
import com.olx.example.model.transaction.Transaction;

public interface ITransactionService {

	public void calcularSaldoDestino(Account acc, float monto,List<Account> accounts);
	
	public void calcularSaldoOrigen(Account acc,float montoTransferido, float impuesto,List<Account> accounts);
	
	public boolean verificarSaldoOrigen(Account acc, float montoTransferido,
			float impuesto, List<Account> accounts);
	
	public boolean validateAccountDestino(Transaction t,List<Account> accounts);
	
	public boolean validateAccountOrigen(Transaction t,List<Account> accounts);
	
	
	
}
