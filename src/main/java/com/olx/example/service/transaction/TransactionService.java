package com.olx.example.service.transaction;

import java.util.List;

import com.olx.example.model.account.Account;
import com.olx.example.model.transaction.Transaction;

public class TransactionService implements ITransactionService {

	@Override
	public void calcularSaldoDestino(Account acc, float monto,List<Account> accounts) {
	 
		float saldo = 0;

		for (Account a : accounts) {

			if (a.getId() == acc.getId()) {
				saldo = monto + a.getSaldo();
				a.setSaldo(saldo);
			}
		}
	}

	
	
	@Override
	public boolean verificarSaldoOrigen(Account acc, float montoTransferido,
			float impuesto, List<Account> accounts) {
		
		
		float saldo = 0;

		for (Account a : accounts) {

			if (a.getId() == acc.getId()) {
				saldo = a.getSaldo() - montoTransferido - impuesto;
				}
		}
		
		
		if (saldo >= 0) {
			return true;
		
		} else {
			return false;
		}
		
	}
	
	
	
	@Override
	public void calcularSaldoOrigen(Account acc, float montoTransferido,
			float impuesto, List<Account> accounts) {
		
		
		float saldo = 0;

		for (Account a : accounts) {

			if (a.getId() == acc.getId()) {
				saldo = a.getSaldo() - montoTransferido - impuesto;
				a.setSaldo(saldo);
			}
		}
	}

	
	
	@Override
	public boolean validateAccountDestino(Transaction t,List<Account> accounts) {
		
		boolean encontro  = false ;
		
		Account a = t.getCuentaDestino();
		
		for (Account cta : accounts) {
			
			if(a.getId() ==cta.getId())
			{
				t.setCuentaDestino(cta); 
				encontro = true;
			}
		}
			
		return  encontro;
				
	}

	
	
	@Override
	public boolean validateAccountOrigen(Transaction t, List<Account> accounts) {
		boolean encontro  = false ;
		
		Account a = t.getCuentaOrigen();
		
		for (Account cta : accounts) {
			
			if(a.getId() ==cta.getId())
			{
				t.setCuentaOrigen(cta); 
				encontro = true;
			}
		}
			
		return  encontro;
	}
		
	 

}
